#!/bin/bash

# script to change the volume using amixer + zenity

current_vol=$(amixer -c 1 get 'Master' |tail -1| sed 's/.*\[\([0-9][0-9]\%\)\].*/\1/g'| sed 's/%//g')

function volume {
VOLVALUE=$(zenity --scale --value=$current_vol --text "set volume")
#CMD="amixer -c 1 set 'Master' $VOLVALUE%"
CMD="amixer -q -D pulse sset Master $VOLVALUE%"
echo $CMD
$CMD
};

function error {
echo "please install zenity"
};

if [[ -f /usr/bin/zenity ]]; 
then echo "OK" 
  volume
else error 
fi
